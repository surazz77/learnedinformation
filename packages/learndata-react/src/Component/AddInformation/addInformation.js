import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles(theme => ({
  button: {
    margin: theme.spacing(1)
  },
  container: {
    display: "flex",
    flexWrap: "wrap",
    margin: "auto 25%"
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1)
  },
  dense: {
    marginTop: theme.spacing(2)
  },
  root: {
    padding: theme.spacing(3, 2),
    display: "flex",
    flexWrap: "wrap",
    margin: "7% 25%"
  },
  header: {
    textAlign: "center"
  }
}));

function AddInformation() {
  const classes = useStyles();
  const handleSubmit = e => {
    e.preventDefault();
    console.log("submited");
  };
  return (
    <React.Fragment>
      <div>
        <Paper className={classes.root}>
          <form noValidate autoComplete="off">
            <Typography variant="h5" component="h3">
              <div className={classes.header}>Learned Information</div>
            </Typography>
            <TextField
              id="outlined-name"
              label="Title"
              className={classes.textField}
              margin="normal"
              fullWidth
              variant="outlined"
            />
            <TextField
              id="outlined-multiline-flexible"
              label="Description"
              multiline
              rowsMax="10"
              fullWidth
              rows="8"
              className={classes.textField}
              margin="normal"
              variant="outlined"
            />
            <Button
              variant="contained"
              color="primary"
              type="submit"
              className={classes.button}
              onClick={handleSubmit}
            >
              submit
            </Button>
          </form>
        </Paper>
      </div>
    </React.Fragment>
  );
}
export default AddInformation;
