import Boom from 'boom';

import Information from '../models/information'

/**
 * Get all information.
 *
 * @returns {Promise}
 */
export function getAllInformation() {
  return Information.fetchAll();
}

/**
 * Get a information.
 *
 * @param   {Number|String}  id
 * @returns {Promise}
 */
export function getInformation(id) {
  return new Information({ id }).fetch().then(information => {
    if (!information) {
      throw Boom.notFound('Information not found');
    }

    return information;
  });
}

/**
 * Create new information.
 *
 * @param   {Object}  information
 * @returns {Promise}
 */
export function createInformation(information) {
  return new Information({ title: information.title,description:information.description }).save();
}

/**
 * Update a information.
 *
 * @param   {Number|String}  id
 * @param   {Object}         information
 * @returns {Promise}
 */
export function updateInformation(id, information) {
  return new Information({ id }).save({ title: information.title,description:information.description });
}

/**
 * Delete a information.
 *
 * @param   {Number|String}  id
 * @returns {Promise}
 */
export function deleteInformation(id) {
  return new Information({ id }).fetch().then(information => information.destroy());
}
