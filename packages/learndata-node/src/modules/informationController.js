import HttpStatus from 'http-status-codes';

import * as informationService from './informationService';

/**
 * Get all informations.
 *
 * @param {Object} req
 * @param {Object} res
 * @param {Function} next
 */
export function fetchAll(req, res, next) {
  informationService
    .getAllInformation()
    .then(data => res.json({ data }))
    .catch(err => next(err));
}

/**
 * Get a information by its id.
 *
 * @param {Object} req
 * @param {Object} res
 * @param {Function} next
 */
export function fetchById(req, res, next) {
  informationService
    .getInformation(req.params.id)
    .then(data => res.json({ data }))
    .catch(err => next(err));
}

/**
 * Create a new information.
 *
 * @param {Object} req
 * @param {Object} res
 * @param {Function} next
 */
export function create(req, res, next) {
  informationService
    .createInformation(req.body)
    .then(data => res.status(HttpStatus.CREATED).json({ data }))
    .catch(err => next(err));
}

/**
 * Update a information.
 *
 * @param {Object} req
 * @param {Object} res
 * @param {Function} next
 */
export function update(req, res, next) {
  informationService
    .updateInformation(req.params.id, req.body)
    .then(data => res.json({ data }))
    .catch(err => next(err));
}

/**
 * Delete a information.
 *
 * @param {Object} req
 * @param {Object} res
 * @param {Function} next
 */
export function deleteInformation(req, res, next) {
  informationService
    .deleteInformation(req.params.id)
    .then(data => res.status(HttpStatus.NO_CONTENT).json({ data }))
    .catch(err => next(err));
}
