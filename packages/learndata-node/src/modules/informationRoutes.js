
import { Router } from 'express';

import * as informationController from './informationController';

const router = Router();

/**
 * GET /api/informations
 */
router.get('/', informationController.fetchAll);

/**
 * GET /api/informations/:id
 */
router.get('/:id', informationController.fetchById);

/**
 * POST /api/informationss
 */
router.post('/',informationController.create);

/**
 * PUT /api/informations/:id
 */
router.put('/:id',informationController.update);

/**
 * DELETE /api/informations/:id
 */
router.delete('/:id',informationController.deleteInformation);

export default router;
